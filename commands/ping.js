exports.data = {
	name: 'Ping',
	command: 'ping',
	description: 'Ping check.',
	group: 'system',
	syntax: 'ping',
	permissions: 3
};

const moment = require('moment');

exports.func = async (receivedmsg, args, bot) => {
	console.log(bot)
	console.log(receivedmsg.member.displayName + " (" + receivedmsg.author.username + "#" + receivedmsg.author.discriminator + ') has pinged the bot in ' + receivedmsg.channel.name + ' on ' + receivedmsg.guild.name);
	const m = await receivedmsg.channel.send(`Ping pongs, but does the Pong ping?.`)
    .then(m => m.edit(`Pong! Latency is ${m.createdTimestamp - receivedmsg.createdTimestamp}ms. API Latency is ${Math.round(bot.ping)}ms`));
};
